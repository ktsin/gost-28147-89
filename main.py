import getpass
import os.path
import hashlib

from typing.io import BinaryIO

SBOX = [
    [
        8, 4, 0xB, 1, 3, 5, 0, 9, 2, 0xE, 0xA, 0xC, 0xD, 6, 7, 0xF
    ],
    [
        0, 1, 2, 0xA, 4, 0xD, 5, 0xC, 9, 7, 3, 0xF, 0xB, 8, 6, 0xE
    ],
    [
        0xE, 0xC, 0, 0xA, 9, 2, 0xD, 0xB, 7, 5, 8, 0xF, 3, 6, 1, 4
    ],
    [
        7, 5, 0, 0xD, 0xB, 6, 1, 2, 3, 0xA, 0xC, 0xF, 4, 0xE, 9, 8
    ],
    [
        2, 7, 0xC, 0xF, 9, 5, 0xA, 0xB, 1, 4, 0, 0xD, 6, 8, 0xE, 3
    ],
    [
        8, 3, 2, 6, 4, 0xD, 0xE, 0xB, 0xC, 1, 7, 0xF, 0xA, 0, 9, 5
    ],
    [
        5, 2, 0xA, 0xB, 9, 1, 0xC, 3, 7, 4, 0xD, 0, 6, 0xF, 8, 0xE
    ],
    [
        0, 4, 0xB, 0xE, 8, 3, 7, 1, 0xA, 2, 9, 6, 0xF, 0xD, 5, 0xC
    ]
]


def print_menu():
    print("\t1. Encrypt file;")
    print("\t2. Decrypt file;")
    print("\t0. Exit;")


def read_bytes_block(file: BinaryIO, block_number: int, shift: int = 8) -> bytes:
    file.seek(shift + block_number * 8)
    result = file.read(8)
    i = bytearray(result)
    result = (0x00000000 | int.from_bytes(result, "little")).to_bytes(8, "little")
    return result


def enter_file_name() -> str:
    file_present = False
    filename: str = ""
    while not file_present:
        filename = input("Type path to file:")
        file_present = os.path.exists(filename) and os.path.isfile(filename)
    return filename


def enter_password() -> bytes:
    pswd = getpass.getpass("\tType your seKr3T password: ")
    pswd = hashlib.sha256(pswd.encode()).digest()
    return pswd


def get_round_key(round_n: int, key: bytes, decrypting: bool = False) -> bytes:
    repeat_number = round_n % 8
    if not decrypting:
        if round_n < 24:
            return key[repeat_number * 4:(repeat_number + 1) * 4]
        else:
            return key[32 - (repeat_number + 1) * 4: 32 - repeat_number * 4]
    else:
        # repeat_number = 8 - repeat_number
        if round_n < 8:
            return key[repeat_number * 4:(repeat_number + 1) * 4]
        else:
            return key[32 - (repeat_number + 1) * 4: 32 - repeat_number * 4]


def shuffle_subblock(subblock: bytes) -> bytes:
    shuffles: int = 0
    for i in range(0, 4):
        byte = subblock[i]
        left = (byte & 0xF0) >> 4
        right = byte & 0x0F
        left = SBOX[2 * i][left]
        right = SBOX[2 * i + 1][right]
        byte = 0
        byte = byte | (left << 4)
        byte = byte | right
        shuffles = (shuffles << i * 4) | byte
    return shuffles.to_bytes(4, "little")


def circular_shift(block: bytes, shift: int) -> bytes:
    i_value = int.from_bytes(block, "little", signed=False)
    left = ((i_value << shift) & 0xFFFF00000000) >> 32
    i_value = i_value << shift & 0xFFFFFFFF
    i_value = i_value | left
    return i_value.to_bytes(4, "little", signed=False)


def encrypt_block(block: bytes, key: bytes):
    print("Encrypting block;")
    A = block[:4]
    B = block[4:]
    for i in range(0, 32):
        round_key = get_round_key(i, key)
        A, B = round_challenge(A, B, round_key)
    return ((int.from_bytes(A, "little") << 32) | int.from_bytes(B, "little")).to_bytes(8, "little")


def encrypt_file():
    f_name = enter_file_name()
    in_file = open(f_name, "rb")
    pswd = enter_password()
    out_file = open(f_name + ".gost", "wb")
    f_size = os.path.getsize(f_name)
    out_file.write(int.to_bytes(f_size, 8, "little"))
    out_file.flush()
    blocks = f_size // 8
    if f_size % 8 > 0:
        blocks = blocks + 1
    for i in range(0, blocks):
        block = encrypt_block(read_bytes_block(in_file, i, shift=0), key=pswd)
        out_file.write(block)
    in_file.close()
    out_file.close()


def decrypt_block(block: bytes, key: bytes):
    print("Decrypting block;")
    A = block[:4]
    B = block[4:]
    for i in range(0, 32):
        round_key = get_round_key(i, key, True)
        A, B = round_challenge(A, B, round_key)
    return ((int.from_bytes(A, "little") << 32) | int.from_bytes(B, "little")).to_bytes(8, "little")


def round_challenge(A, B, round_key):
    sub = B
    sub = (int.from_bytes(sub, "little") + int.from_bytes(round_key, "little")) % (2 ** 32)
    sub = shuffle_subblock(sub.to_bytes(length=4, byteorder="little", signed=False))
    sub = circular_shift(sub, 11)
    sub = (int.from_bytes(A, "little") ^ int.from_bytes(sub, "little")).to_bytes(4, "little")
    A = B
    B = sub
    return A, B


def decrypt_file():
    f_name = enter_file_name()
    in_file = open(f_name, "rb")
    pswd = enter_password()
    out_file = open(f_name.replace(".gost", ""), "wb")
    f_size = int.from_bytes(in_file.read(8), "little")
    blocks = f_size // 8
    if f_size % 8 > 0:
        blocks = blocks + 1
    for i in range(0, blocks):
        block = decrypt_block(read_bytes_block(in_file, i), key=pswd)
        out_file.write(block)
    in_file.close()
    out_file.close()


def main():
    exit_flag = False
    while not exit_flag:
        print_menu()
        u_input = int(input("\tYour input: "))
        if u_input == 1:
            encrypt_file()
        else:
            if u_input == 2:
                decrypt_file()
            else:
                exit_flag = True


if __name__ == '__main__':
    main()